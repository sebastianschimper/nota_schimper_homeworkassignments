local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    local armPeeweeUnits = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name == "armpw") then
            armPeeweeUnits[#armPeeweeUnits + 1] = unitsArg[i]
        end
    end

    return armPeeweeUnits

end