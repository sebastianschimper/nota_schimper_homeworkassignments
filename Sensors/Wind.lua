local sensorInfo = {
    name = "windDirection",
    desc = "Returns the direction of the wind.",
    author = "PepeAmpere and Sebastian Schimper",
    date = "2019-04-12",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
    local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()

    --[[
    return {
        dirX = dirX,
        dirY = dirY,
        dirZ = dirZ,
        strength = strength,
        normDirX = normDirX,
        normDirY = normDirY,
        normDirZ = normDirZ,
    }
    ]]

    return Vec3(dirX, dirY, dirZ)
end