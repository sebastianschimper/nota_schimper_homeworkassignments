local sensorInfo = {
	name = "ttdr_ScoutTargetPositions",
	desc = "Prepare target positions",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(listOfUnits, groupSize)
    local positions = {}
    local groupIndex = 1
    local subgroupIndex = 1
    
    for i=1, #listOfUnits do
        if positions[groupIndex] == nil then
            positions[groupIndex] = {}
        end

        positions[groupIndex][subgroupIndex] = Vec3(Game.mapSizeX / #listOfUnits * i )

        subgroupIndex = subgroupIndex + 1
        if (subgroupIndex > groupSize) then
            subgroupIndex = 1
            groupIndex = groupIndex + 1
        end
        
    end
    
    return positions
end