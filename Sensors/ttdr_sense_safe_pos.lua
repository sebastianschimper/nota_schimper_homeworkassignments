local sensorInfo = {
	name = "Find Safe Points",
	desc = "Returns coordinates with max y-value",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- actual array with safe tiles
local allTiles = {}
local safeTiles = {}
local maxZTiles = {}


return function()


	-- map and tile information
	local tile_size = 200 -- elmo unit
	local map_tiles_X = math.ceil(Game.mapSizeX / tile_size)
	local map_tiles_Z = math.ceil(Game.mapSizeZ / tile_size)
    local map_size = map_tiles_X * map_tiles_Z
    
    local safeMaxZ = 750


	--- tile info
	local tileX = nil
	local tileY = nil
	local tileZ = nil

    local x = -tile_size
    local z = 0
    
    local minY = 1000000000
		
	--Spring.Echo(map_size)

    for i=1, map_size do
        --split the map into tiles 
		if (i % map_tiles_Z == 1) then
			z = 0
			x = x + tile_size
		else
			z = z + tile_size
        end
        
        tileX = x + (tile_size/2)
        tileZ = z + (tile_size/2)
        tileY = Spring.GetGroundHeight(tileX, tileZ)
        
        if tileY < minY then
            minY = tileY
        end

        local tilePosition = Vec3(tileX, tileY, tileZ)

        --[[
        local tilePosition = {
            x = tileX, 
            y = tileY, 
            z = tileZ
        }
        ]]--
        allTiles[#allTiles + 1] = tilePosition
    
    end

    for j=1, #allTiles do
        if allTiles[j].y >= minY and allTiles[j].y <= minY + 50 then
            safeTiles[#safeTiles + 1] = allTiles[j]
        end
    end

    for l=1, #maxZTiles do
        safeTiles[#safeTiles + 1] = maxZTiles[l]
    end

    return safeTiles

end