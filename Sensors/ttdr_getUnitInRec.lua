local sensorInfo = {
	name = "Find Safe Points",
	desc = "Returns coordinates with max y-value",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(thisUnit)

    local x,y,z = Spring.GetUnitPosition(thisUnit)

    local unitsAtPos = Spring.GetUnitsInRectangle(x-20, z-20, x+20, z+20)

    return unitsAtPos
	

end