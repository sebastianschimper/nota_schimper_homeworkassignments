local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()

    local mapSizeX = Game.mapSizeX
    local mapSizeZ = Game.mapSizeZ

    local startX = mapSizeX / 2
    local startZ = 0
    local startY = Spring.GetGroundHeight(startX, startZ)

    return Vec3(startX, startY, startZ)

end