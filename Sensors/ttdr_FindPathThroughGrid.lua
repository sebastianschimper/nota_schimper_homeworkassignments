local sensorInfo = {
	name = "FindPathThroughGrid",
	desc = "",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function copyList(list)
	local newList = {}

	for i = 1, #list, 1 do
		newList[i] = list[i]
	end
	return newList
end

-- @description 
return function(gridConections, from, to)
	if not from or not to then
		return {}
	end

	local minDistFrom, minDistTo = math.huge, math.huge
	local closestPointFrom, closestPointTo

	for pos,_ in pairs(gridConections) do
		local dist = math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
		if dist < minDistFrom then
			minDistFrom, closestPointFrom = dist, pos
		end
	end

	for pos,_ in pairs(gridConections) do
		local dist = math.sqrt((pos.x - to.x) * (pos.x - to.x) + (pos.z - to.z) * (pos.z - to.z))
		if dist < minDistTo then
			minDistTo, closestPointTo = dist, pos
		end
	end


	local path = {}

	--bfs
	local queue = {}
	local goal = false

	queue[#queue + 1] = {}
	queue[#queue][#queue[#queue] + 1] = closestPointFrom

	local QIndex = 1
	while QIndex <= #queue do
		if goal then
			break
		end

		local pathExpanding = queue[QIndex]
		local pointsNeghbours = gridConections[pathExpanding[#pathExpanding]]

		for neighIndex = 1, #pointsNeghbours, 1 do
			if pointsNeghbours[neighIndex] == closestPointTo then
				goal = true
				pathExpanding[#pathExpanding + 1] = pointsNeghbours[neighIndex]
				path = pathExpanding
				break
			end

			found = false
			for _,QVal in pairs(queue) do 	--if not in queue
 				for QValIndex = 1, #QVal, 1 do
 					if QVal[QValIndex] == pointsNeghbours[neighIndex] then
						found = true
					end
				end
			end

			if not found then	--if not already expanded, put into queue
				queue[#queue + 1] = copyList(pathExpanding)
				queue[#queue][#queue[#queue] + 1] = pointsNeghbours[neighIndex]
			end
		end
		QIndex = QIndex + 1
	end

	path[#path + 1] = to

	return path
end
