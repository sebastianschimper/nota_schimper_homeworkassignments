local sensorInfo = {
	name = "SenseEnemyWeaponRange",
	desc = "Returns shooting range of enemy's weapons",
	author = "Dario & Sebby",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end


return function(enemyUnits) 
    enemyPos = {}

    for i=1, #enemyUnits do
        local x, y, z = Spring.GetUnitPosition(enemyUnits[i])
        local newIndex =  enemyUnits[i]
        local thisEnemyPos = Vec3(x,y,z)
        if has_value(enemyPos, enemyPos[newIndex]) == false then
            enemyPos[newIndex] = thisEnemyPos
        end
    end

    return enemyPos
end