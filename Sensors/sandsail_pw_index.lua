local sensorInfo = {
    name = "PW_index",
    desc = "Returns an array with 4 positions.",
    author = "PepeAmpere and Sebastian Schimper",
    date = "2019-04-12",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(unitArray)
    
    local groupDef = {}

    for i=1, #unitArray do
        local thisUnitID = unitArray[i]
        groupDef[thisUnitID] = i
    end
    
    return groupDef

end