local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()

    local mapSizeX = Game.mapSizeX
    local mapSizeZ = Game.mapSizeZ

    local targetX = mapSizeX / 2
    local targetZ = mapSizeZ
    local targetY = Spring.GetGroundHeight(targetX, targetZ)

    return Vec3(targetX, targetY, targetZ)

end