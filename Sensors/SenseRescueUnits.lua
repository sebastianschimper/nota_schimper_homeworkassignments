local sensorInfo = {
	name = "SenseRescueUnits",
	desc = "Returns all units to rescure",
	author = "Sebastian Schimper",
	date = "2019",
}

--debugg
local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function euclidean_dist(x,y,a,b)
	local res = math.sqrt(math.pow(x-a,2) + math.pow(y-b,2))
	return res
end

-- helper function to sort table
-- stolen from here: https://forums.coronalabs.com/topic/46704-how-to-sort-a-table-on-2-values/
local function tableSortDist (a, b )
    if (a.dist < b.dist) then
           return true
        elseif (a.dist > b.dist) then
            return false
        else
              return a.id < b.id
        end
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

return function(safeAreaCenter, safeAreaRadius)

	local myTeamID = Spring.GetMyTeamID()
	local unitsToRescue = {}
	local unitsToRescueSortedAndFiltered = {}
	local unitsToRescueSorted = {}

	local thisTeamUnits = Spring.GetTeamUnits(myTeamID)

	local allUnitsInSaveArea = Spring.GetUnitsInSphere(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z, safeAreaRadius)
	local unitsToSaveInArea = {}

	--[[
	-- filter units in safe area
	for i=1, #allUnitsInSaveArea do
		local thisSaveUnitId = allUnitsInSaveArea[i]
		local thisSaveUnitDefId = Spring.GetUnitDefID(thisSaveUnitId)

		if(UnitDefs[thisSaveUnitDefId].name ~= "armpeep") and (UnitDefs[thisSaveUnitDefId].name ~= "armatlas") then
			unitsToSaveInArea[#unitsToSaveInArea + 1] = allUnitsInSaveArea[i]
		end
	end
	]]--
		
	for i=1, #thisTeamUnits do
		local thisUnitID = thisTeamUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)

		-- get all units to rescue and compute their euclidean distance to the save area
		if not has_value(allUnitsInSaveArea, thisUnitID) then
			if (UnitDefs[thisUnitDefID].name ~= "corrad") and (UnitDefs[thisUnitDefID].name ~= "armwin") and (UnitDefs[thisUnitDefID].name ~= "armpeep") and (UnitDefs[thisUnitDefID].name ~= "armatlas") and (UnitDefs[thisUnitDefID].name ~= "armrad") then
			
				local pointX, pointY, pointZ = Spring.GetUnitPosition(thisTeamUnits[i])		
				local dist = euclidean_dist(safeAreaCenter.x, safeAreaCenter.z, pointX, pointZ)
				unitsToRescue[#unitsToRescue + 1] = {dist = dist, id = thisTeamUnits[i]}				
					
			end	
		end
	end

	--sort array according to distance to center
	table.sort(unitsToRescue, tableSortDist)

	-- just keep id and return
	for i=1, #unitsToRescue do
		unitsToRescueSorted[#unitsToRescueSorted + 1] = unitsToRescue[i].id
	end

		-- filter for units already in safe area
	if(#unitsToSaveInArea > 0) then
		for i=1, #unitsToSaveInArea do
			for j=1, #unitsToRescueSorted do
				if(unitsToSaveInArea[i] == unitsToRescueSorted[j]) then
					unitsToRescueSorted[j] = nil
				end
			end
		end
	end

	-- last but not least, update the table
	for i=1, #unitsToRescueSorted do
		if(unitsToRescueSorted[i] ~= nil) then
			unitsToRescueSortedAndFiltered[#unitsToRescueSortedAndFiltered + 1] = unitsToRescueSorted[i]
		end
	end


	
	return unitsToRescueSortedAndFiltered
end