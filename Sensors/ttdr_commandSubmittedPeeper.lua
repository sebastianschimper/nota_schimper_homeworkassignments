local sensorInfo = {
	name = "Sense Enemy Units",
	desc = "Stores all enemies in the area",
	author = "Team 1, Group A",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(allPeeperUnits)

    local commandAlreadySubmitted = {}

    for i=1, #allPeeperUnits do
            commandAlreadySubmitted[allPeeperUnits[i]] = false
        
    end

    return commandAlreadySubmitted
end