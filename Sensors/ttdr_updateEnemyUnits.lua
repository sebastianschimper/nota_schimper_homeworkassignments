local sensorInfo = {
	name = "Sense Enemy Units",
	desc = "Stores all enemies in the area",
	author = "Team 1, Group A",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

-- takes an empty array as input
return function(enemyUnits)
    local updatedEnemyUnits = enemyUnits
	local enemyTeams = Sensors.core.EnemyTeamIDs()
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
        for u=1, #thisTeamUnits do
            if has_value(updatedEnemyUnits, thisTeamUnits[u]) == false then
                updatedEnemyUnits[#updatedEnemyUnits + 1] = thisTeamUnits[u]
            end
		end
	end
	
	return updatedEnemyUnits
end