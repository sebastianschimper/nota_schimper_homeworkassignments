local sensorInfo = {
	name = "pos",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()
    local posArray = {
        [1] = Vec3(-10,0,-10),
        [2] = Vec3(-10,0,10),
        [3] = Vec3(10,0,-10),
        [4] = Vec3(10,0,10)
    }

    --[[
    posArray[1] = Vec3(-10,0,-10)
    posArray[2] = Vec3(10,0,-10)
    posArray[3] = Vec3(-10,0,10)
    posArray[4] = Vec3(10,0,10)
    ]]--

    return posArray
end