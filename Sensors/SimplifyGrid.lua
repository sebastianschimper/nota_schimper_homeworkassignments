local sensorInfo = {
	name = "CreateGrid",
	desc = "Simplify Grid",
	author = "Sebastian Schimper",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(thisGrid, safeAreaCenter)

	if thisGrid == nil then
		return nil
	end
	
	local simplifiedGrid = {}

	--- path to the south
	local startPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z)
	local secondStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400)
	local thirdStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *2)
	local fourthStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *3)
	local fifthStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *4)
	local sixthStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *5)
	local seventhStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *6)
	local eighthStartPoint = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z + 400 *7)
	simplifiedGrid[1] = startPoint
	simplifiedGrid[2] = secondStartPoint
	simplifiedGrid[3] = thirdStartPoint
	simplifiedGrid[4] = fourthStartPoint
	simplifiedGrid[5] = fifthStartPoint
	simplifiedGrid[6] = sixthStartPoint
	simplifiedGrid[7] = seventhStartPoint
	simplifiedGrid[8] = eighthStartPoint

	--path to the west
	local secondStartPointWest = Vec3(safeAreaCenter.x - 400, safeAreaCenter.y, safeAreaCenter.z)
	local thirdStartPointWest = Vec3(safeAreaCenter.x - 400 * 2, safeAreaCenter.y, safeAreaCenter.z)
	local fourthStartPointWest = Vec3(safeAreaCenter.x - 400 * 3, safeAreaCenter.y, safeAreaCenter.z)
	local fifthStartPointWest = Vec3(safeAreaCenter.x - 400 * 4, safeAreaCenter.y, safeAreaCenter.z)
	local sixthStartPointWest = Vec3(safeAreaCenter.x - 400 * 5, safeAreaCenter.y, safeAreaCenter.z)
	local seventhStartPointWest = Vec3(safeAreaCenter.x - 400 * 6, safeAreaCenter.y, safeAreaCenter.z)
	local eighthStartPointWest = Vec3(safeAreaCenter.x - 400 * 7, safeAreaCenter.y, safeAreaCenter.z)
	local ninethStartPointWest = Vec3(safeAreaCenter.x - 400 * 8, safeAreaCenter.y, safeAreaCenter.z)

	simplifiedGrid[9] = secondStartPointWest
	simplifiedGrid[10] = thirdStartPointWest
	simplifiedGrid[11] = fourthStartPointWest
	simplifiedGrid[12] = fifthStartPointWest
	simplifiedGrid[13] = sixthStartPointWest
	simplifiedGrid[14] = seventhStartPointWest
	simplifiedGrid[15] = eighthStartPointWest
	simplifiedGrid[16] = ninethStartPointWest

	--path to nord
	local secondStartPointNorth = Vec3(safeAreaCenter.x - 400 * 8, safeAreaCenter.y, safeAreaCenter.z - 400)
	local thirdStartPointNorth = Vec3(safeAreaCenter.x - 400 * 8, safeAreaCenter.y, safeAreaCenter.z - 400 * 2)
	--local fourthStartPointNorth = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z - 400 *3)
	--local fifthStartPointNorth = Vec3(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z - 400 *4)

	simplifiedGrid[17] = secondStartPointNorth
	simplifiedGrid[18] = thirdStartPointNorth
	--simplifiedGrid[19] = fourthStartPointNorth
	--simplifiedGrid[20] = fifthStartPointNorth

	for k, v in pairs(thisGrid) do
		local thisPoint = k
		if #v <= 2 then
			simplifiedGrid[#simplifiedGrid + 1] = thisPoint
		end
	end

	return simplifiedGrid
end