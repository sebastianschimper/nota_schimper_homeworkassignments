local sensorInfo = {
	name = "Find Platforms",
	desc = "Returns coordinates with max y-value",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- map and tile information
local tile_size = 128 -- elmo unit
local map_tiles_X = math.ceil(Game.mapSizeX / tile_size)
local map_tiles_Z = math.ceil(Game.mapSizeZ / tile_size)
local map_size = map_tiles_X * map_tiles_Z


return function()
	
	--hight of 
	local height_value = nil

	--arrays for storing pts
	local potential_platform_tiles = {}
	local highest_tiles = {}
	local x = -tile_size
	local z = 0
	local array_iterator = 1

	--platform positions (0,0,0 for now, change later)
	local platform_X = nil
	local platform_Y = nil
	local platform_Z = nil
	
	--- maximun Y heigth value
	local max_Y = 0
	
	--- check any pt on map, if hight is over 0 then store the pt in array
	for i=1, map_size do
	
		--- Yulia's idea: split the map into tiles 
		if (i % map_tiles_Z == 1) then
			z = 0
			x = x + tile_size
		else
			z = z + tile_size
		end
	
		platform_X = x + (tile_size/2)
		platform_Z = z + (tile_size/2)
		platform_Y = Spring.GetGroundHeight(platform_X, platform_Z)
		
		---store max Y value
		if (platform_Y > max_Y) then
			max_Y = platform_Y
		end
		
		--- store all values
		potential_platform_tiles[array_iterator] = {
			x = platform_X,
			y = platform_Y,
			z = platform_Z,
		}			
		array_iterator = array_iterator + 1

	end -- end loop
	
	--- store highest tiles in 2nd array
	array_iterator = 1
	for i=1, #potential_platform_tiles do
		if(potential_platform_tiles[i].y == max_Y) then
			highest_tiles[array_iterator] = potential_platform_tiles[i]
			array_iterator = array_iterator + 1
		end
	end
	
	return highest_tiles
	
end

