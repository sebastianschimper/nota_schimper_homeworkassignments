local sensorInfo = {
	name = "SenseRescueUnits",
	desc = "Returns all units to rescure",
	author = "Sebastian Schimper",
	date = "2019",
}

--debugg
local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(safeAreaCenter, safeAreaRadius)

	local myTeamID = Spring.GetMyTeamID()
	local groundUnits = {}

	local thisTeamUnits = Spring.GetTeamUnits(myTeamID)


		
	for i=1, #thisTeamUnits do
		local thisUnitID = thisTeamUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)

		-- get all units 
			if (UnitDefs[thisUnitDefID].name ~= "corrad") and (UnitDefs[thisUnitDefID].name ~= "armwin") and (UnitDefs[thisUnitDefID].name ~= "armpeep") and (UnitDefs[thisUnitDefID].name ~= "armatlas") and (UnitDefs[thisUnitDefID].name ~= "armrad") then
			
				groundUnits[#groundUnits + 1] = thisTeamUnits[i]				
					
			end	

	end

	
	return groundUnits
end