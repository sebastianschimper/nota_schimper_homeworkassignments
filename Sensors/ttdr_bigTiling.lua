local sensorInfo = {
	name = "Create Big Tiles",
	desc = "Returns coordinates with max y-value",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- actual array with safe tiles
local allTiles = {}


return function()


	-- map and tile information
	local tile_size = Game.mapSizeX / 12  -- elmo unit
	local map_tiles_X = math.ceil(Game.mapSizeX / tile_size)
	local map_tiles_Z = math.ceil(Game.mapSizeZ / tile_size)
    local map_size = map_tiles_X * map_tiles_Z


	--- tile info
	local tileX = nil
	local tileY = nil
	local tileZ = nil

    local x = -tile_size
    local z = 0
    
		
	--Spring.Echo(map_size)

    for i=1, map_size do
        --split the map into tiles 
		if (i % map_tiles_Z == 1) then
			z = 0
			x = x + tile_size
		else
			z = z + tile_size
        end
        
        tileX = x + (tile_size/2)
        tileZ = z + (tile_size/2)
        tileY = Spring.GetGroundHeight(tileX, tileZ)
        

        local tilePosition = Vec3(tileX, tileY, tileZ)

        --[[
        local tilePosition = {
            x = tileX, 
            y = tileY, 
            z = tileZ
        }
        ]]--
        allTiles[#allTiles + 1] = tilePosition
    
    end

    --return safeTiles
    return allTiles

end