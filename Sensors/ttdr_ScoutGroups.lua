local sensorInfo = {
	name = "ttdr_ScoutGroups",
	desc = "Prepare target positions",
	author = "Sebastian Schimper",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(listOfUnits, groupSize)
    local scoutUnits = {}
    local groupIndex = 1
    local subgroupIndex = 1
    for i=1, #listOfUnits do
        if scoutUnits[groupIndex] == nil then
            scoutUnits[groupIndex] = {}
        end

        scoutUnits[groupIndex][subgroupIndex] = listOfUnits[i]

        subgroupIndex = subgroupIndex + 1
        if (subgroupIndex > groupSize) then
            subgroupIndex = 1
            groupIndex = groupIndex + 1
        end
    end

    return scoutUnits
	
end