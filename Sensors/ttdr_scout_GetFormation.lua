local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(thisScouts)

    local mapSizeX = Game.mapSizeX
    local mapSizeZ = Game.mapSizeZ

    local amount = #thisScouts

    local distance = mapSizeX / amount

    local table = {}

    for i=1, #thisScouts do
        --local x,y,z = Spring.GetUnitPosition(thisScouts[i])

        table[i] = Vec3((i - 1) * distance,0,0)
    end

    return table

end