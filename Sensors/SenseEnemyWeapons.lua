local sensorInfo = {
	name = "SenseEnemyWeaponRange",
	desc = "Returns shooting range of enemy's weapons",
	author = "Dario & Sebby",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(enemyUnits)


	local enemyWeapons = {}
	--[[
	

	for i=1, #enemyUnits do

		local thisUnitDefID = enemyUnits[i]

		local weaponsTable  = UnitDefs[thisUnitDefID].weapons

		if (weaponsTable ~= nil)then

			for indexOfWeapons, weaponInfo in pairs (weaponsTable) do

				local thisWeaponDefID = weaponInfo.weaponDef 
					
				if(thisWeaponDefID ~= nil )then
					local thisWeaponDef = WeaponDefs[thisWeaponDefID]					
					if(thisWeaponDef ~= nil )then
						enemyWeapons[#enemyWeapons + 1] = thisWeaponDef
					end
				end
			end
		end
	end

	return enemyWeapons

	--]]

	for index,enemyUnitID in pairs (enemyUnits)do
		local thisUnitDefID = Spring.GetUnitDefID(enemyUnitID)
		if(thisUnitDefID ~= nil)then
			local weaponsTable  = UnitDefs[thisUnitDefID].weapons
			if (weaponsTable ~= nil)then
					for indexOfWeapons, weaponInfo in pairs (weaponsTable) do
					local thisWeaponDefID =  weaponInfo.weaponDef 
					if(thisWeaponDefID ~= nil )then
							local thisWeaponDef=WeaponDefs[thisWeaponDefID]					
						if(thisWeaponDef ~= nil )then
							
							enemyWeapons[#enemyWeapons + 1] = thisWeaponDef
						end
					end
				end
			end
		end
	end

	return enemyWeapons
end
