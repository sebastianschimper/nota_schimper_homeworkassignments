local sensorInfo = {
	name = "Sense Random Point in Safe Area",
	desc = "Returns all units to rescure",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(thisUnits)

    local minPosX = 100000000000000
    local leftMost = nil

    for i=1, #thisUnits do
        local posX, posY, posZ = Spring.GetUnitPosition(thisUnits[i])
        if posX <= minPosX then
            minPosX = posX
            leftMost = thisUnits[i]
        end
    end 

    return leftMost

end