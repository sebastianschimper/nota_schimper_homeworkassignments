local sensorInfo = {
	name = "sense free spot",
	desc = "sense free spot in safe area",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(thisUnit)

    local posX, posY, posZ = Spring.GetUnitPosition(thisUnit)

    local unloadAreaOccupied = Spring.GetUnitsInRectangle(posX - 20, posZ - 20, posX + 20, posZ + 20)

	-- if there's more than the atlas unit itself and the transported unit
	-- then look for another place
	if #unloadAreaOccupied > 2 then
		return false
	else
		return true
	end
	



end