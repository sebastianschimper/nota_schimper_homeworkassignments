local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    local atlasUnits = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name == "armatlas") then
            atlasUnits[#atlasUnits + 1] = unitsArg[i]
        end
    end

    return atlasUnits

end