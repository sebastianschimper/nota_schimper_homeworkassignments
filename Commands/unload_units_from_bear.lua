--- takes loaction to unload troops as inpost 
function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload troops on hill"
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)
	
	local bear_unit = {}
	
	local radius = 100 -- hardcoded
	
	local cmdID = CMD.UNLOAD_UNITS
	
	-- locate bear
	for i=1, #units do
		local thisUnitID = units[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
	
		if (UnitDefs[thisUnitDefID].name == "armthovr")then
			bear_unit[#bear_unit + 1] = units[i]
		end
	end
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(bear_unit[1])
	
	SpringGiveOrderToUnit(bear_unit[1], cmdID, {pointX, pointY, pointZ, radius},  {})
	
	if (#Spring.GetUnitIsTransporting(bear_unit[1]) == 0) then 
		return RUNNING
	else
		return SUCCESS
	end
	
end