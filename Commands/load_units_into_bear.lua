--- takes units to load into bear as parameters 
function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "unitsToLoad",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)

	local unitsToLoad = parameter.unitsToLoad -- array
	
	local bear_unit = {}
	local warrior_units = {}
	
	local cmdID = CMD.LOAD_UNITS
	
	for i=1, #unitsToLoad do
		local thisUnitID = unitsToLoad[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
		
		if (UnitDefs[thisUnitDefID].name == "armthovr")then
			bear_unit[#bear_unit + 1] = unitsToLoad[i]
		elseif (UnitDefs[thisUnitDefID].name == "armwar") then
			warrior_units[#warrior_units + 1] = unitsToLoad[i]
		end
		
	end
	
	local posX, posY, posZ = SpringGetUnitPosition(warrior_units[2])
	
	local radius = 150 -- harcoded

	SpringGiveOrderToUnit(bear_unit[1], cmdID, {posX, posY, posZ, radius},  {}) --- load units into bear 

	if (#Spring.GetUnitIsTransporting(bear_unit[1]) ~= 3) then
		return RUNNING
	else 
		return SUCCESS
	end
	
end