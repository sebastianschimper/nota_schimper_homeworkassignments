-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module
local mi = VFS.Include("LuaUI/BETS/projects/core/Sensors/MissionInfo.lua")
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS,
		parameterDefs = {
			{ 
				name = "TransportUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{units}",
			},
			{ 
				name = "UnitsToTransport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{units}",
			},
			{
				name = "TargetArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{targetArea}",
			}
		}
	}
end

local gup = Spring.GetUnitPosition;
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function endMission()
	message.SendRules({
		subject = "manualMissionEnd",
		data = {},
	})
end

UnitsStates = {} -- make it global to be saved

local function RandomPointInCircle(x,y,r)
  r = math.sqrt(math.random())*r --put random distance from center
  local theta = math.random()*2*math.pi -- random angle
  return { x=x+r*math.cos(theta), y=y+r*math.sin(theta) }
end

function Run(self, units, parameter)
	local TransportUnits = parameter.TransportUnits
	local UnitsToTransport = parameter.UnitsToTransport
	local TargetArea = parameter.TargetArea

	if(TransportUnits.length == 0) then
		return SUCCESS
	end

    --[[
	if(mi().score >= 100) then
		endMission()
		return SUCCESS
    end
    ]]--
	
	local move = CMD.MOVE
	local load = CMD.LOAD_UNITS
	local unload = CMD.UNLOAD_UNITS
	local wait = CMD.TIMEWAIT

	--if(CheckAllOk())
    local executingCommand = false
    if (TransportUnits == nil) then
        return SUCCESS
    end
	for i = 1, #TransportUnits do

		if(Spring.ValidUnitID(TransportUnits[i]) ~= false and #UnitsToTransport > 0) then
			--if(UnitsStates[i] == nil ) then
			--	UnitsStates[i] = "loading"
				local cmdQueue = Spring.GetUnitCommands(TransportUnits[i], 1);
				if (#cmdQueue>0) then 
					executingCommand = true
					--local cmdTag = cmdQueue[1].tag
					--Spring.Echo(cmdTag)
				else
					if(UnitsStates[i] == nil) then
						UnitsStates[i] = "load"
						local unit = UnitsToTransport[1]
						table.remove(UnitsToTransport, 1)
						local x,y,z = gup(unit)

--						local rndInCircle = RandomPointInCircle(TargetArea.center.x, TargetArea.center.z, TargetArea.radius)
--							SpringGiveOrderToUnit(
--								TransportUnits[i], 
--								move, 
--								{ rndInCircle.x, 0, rndInCircle.y, TargetArea.radius }, 
--								{"shift"}) -- Go back

						SpringGiveOrderToUnit(
							TransportUnits[i], 
							load, 
							{ x,y,z, 2}, 
							{"shift"}) --Load Unit
						executingCommand = true
						Spring.Echo("LOAD")
					else
						if(UnitsStates[i] == "load") then
							UnitsStates[i] = "moveback"
							local rndInCircle = RandomPointInCircle(TargetArea.center.x, TargetArea.center.z, TargetArea.radius)
							SpringGiveOrderToUnit(
								TransportUnits[i], 
								move, 
								{ rndInCircle.x, 0, rndInCircle.y, TargetArea.radius }, 
								{"shift"}) -- Go back
							executingCommand = true
							Spring.Echo("MOVE")
						else
							if(UnitsStates[i] == "moveback") then
								UnitsStates[i] = "wait"
								SpringGiveOrderToUnit(
									TransportUnits[i], 
									wait, 
									{ 10 }, 
									{"shift"}) -- Wait
								executingCommand = true
								Spring.Echo("WAIT")
							else
								if(UnitsStates[i]) == "wait" then
									UnitsStates[i] = nil
									SpringGiveOrderToUnit(
									TransportUnits[i], 
									unload, 
									{ TargetArea.center.x, 0,TargetArea.center.z, TargetArea.radius}, 
									{"shift"}) -- Unload Unit
									Spring.Echo("UNLOAD")
								end
							end
						end
					end
				end
				
				--unitCol=unitCol+1
			--else
				--return SUCCESS
			--end
		end
	end
	--if(executingCommand) then
		return RUNNING
	--else 
	--	return SUCCESS
	--end
end