function getInfo()
	return {
		onNoUnits = SUCCESS,
        tooltip = "Move with the wind. ",
        parameterDefs = {
            { 
				name = "wind",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
        }
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)

    if not parameter.wind then
        return FAILURE
    end

    local wind = parameter.wind

    local x,y,z = Spring.GetUnitPosition(units[1])
	local myPosition = Vec3(x,y,z)
	local positionWhereIGo = myPosition + wind * 5
	
	if (positionWhereIGo.x < 0) then positionWhereIGo.x = 0 end
	if (positionWhereIGo.z < 0) then positionWhereIGo.z = 0 end
	if (positionWhereIGo.x > Game.mapSizeX) then positionWhereIGo.x = Game.mapSizeX end
	if (positionWhereIGo.z > Game.mapSizeZ) then positionWhereIGo.z = Game.mapSizeZ end

    -- Spring.GiveOrderToUnit(units[1], CMD.MOVE, {x + wind.x, y + wind.y, z + wind.z}, {})
    Spring.GiveOrderToUnit(units[1], CMD.MOVE, positionWhereIGo:AsSpringVector(), {})

    return RUNNING

end
