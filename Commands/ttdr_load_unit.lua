function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "thisUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToPickUp",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringValidUnitID = Spring.ValidUnitID

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
    --local listOfUnitsToRescue = parameter.unitToPickUp 
		local atlasUnit = parameter.thisUnit
		local unitToPickUp = parameter.unitToPickUp
		
		if not parameter.thisUnit then
			return FAILURE
		end


		local cmdID = CMD.LOAD_UNITS
		local commandQueue = Spring.GetUnitCommands(atlasUnit, 1)

		if(#Spring.GetUnitIsTransporting(atlasUnit) == 1) then
			return SUCCESS	
		end
		

		
		--if (#commandQueue == 0 ) then
			local posX, posY, posZ = SpringGetUnitPosition(unitToPickUp)


			SpringGiveOrderToUnit(atlasUnit, cmdID, {posX, posY, posZ, 55},  {}) --- load unit into atlas 
		--end

		return RUNNING

	end