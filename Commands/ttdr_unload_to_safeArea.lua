function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlasUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)
	local atlasUnit = parameter.atlasUnit

	if not parameter.atlasUnit then
		return FAILURE
	end


	local cmdID = CMD.UNLOAD_UNIT

	
	local posX, posY, posZ = SpringGetUnitPosition(atlasUnit)
	local atlasPos = Vec3(posX, posY, posZ)

	if Spring.ValidUnitID(atlasUnit) and #Spring.GetUnitIsTransporting(atlasUnit) ~= 0 then
		SpringGiveOrderToUnit(atlasUnit, cmdID, {posX, posY, posZ},  {})
	end

	if not Spring.ValidUnitID(atlasUnit) then
		return FAILURE
	end

	if #Spring.GetUnitIsTransporting(atlasUnit) == 0 then
		return SUCCESS
	end

	return RUNNING

end