
function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to own position",
		parameterDefs = {
            { 
				name = "groundUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 50
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	if not parameter.groundUnits then
		return FAILURE
	end

	local groundUnits = parameter.groundUnits

    local count = 0
    for i=1, #groundUnits do
        local x,y,z = Spring.GetUnitPosition(groundUnits[i])

        Spring.GiveOrderToUnit(groundUnits[i], CMD.MOVE, {x,y,z}, {})
        count = count + 1
    end

    if count == #groundUnits then
        return SUCCESS
    end

	return RUNNING
end