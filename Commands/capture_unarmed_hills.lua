--- send the three peewees up the unarmed hills
function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Capture the hills.",
		parameterDefs = {
			{ 
				name = "listUAHills",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	-- local listOfUnits = parameter.listOfUnits -- array
	local listUAHills = parameter.listUAHills -- array
	local listOfUnits = units

	local cmdID = CMD.MOVE

	-- sends unit 1 to hill 1, unit 2 to hill 2, 3 to 3
	for i=1, #listOfUnits do
		if (i > 3) then --- assume there is no more than 3 peewees
			break
		end
		SpringGiveOrderToUnit(listOfUnits[i], cmdID, {listUAHills[i].x, listUAHills[i].y, listUAHills[i].z}, {})
	end

	for i=1, #listOfUnits do
		if (SpringGetUnitPosition(listOfUnits[i]) == {listUAHills[i].x, listUAHills[i].y, listUAHills[i].z}) then 
			return SUCCESS 
		else
			return RUNNING
		end
	end
	
end




function Reset(self)
	ClearState(self)
end
