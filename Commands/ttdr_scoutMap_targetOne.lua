function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move custom group evenly accorss the map.",
        parameterDefs = {            
            { 
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)

    -- store map dimensions
    local mapSizeX = Game.mapSizeX 
    local mapSizeZ = Game.mapSizeZ

    -- store command
    local cmdID = CMD.MOVE 

    -- transport units and amount
    local scoutID = parameter.unitID
    local finalPosition = parameter.position

    local x, y, z = Spring.GetUnitPosition(scoutID)
    local myPosition = Vec3(x,y,z)
    local nextPosition = myPosition + ((finalPosition - myPosition):Normalize() * 200)

    SpringGiveOrderToUnit(scoutID, cmdID, nextPosition:AsSpringVector(), {})

    local distance = nextPosition:Distance(myPosition)
    if distance < 50 then
        return SUCCESS
    end
    
    return RUNNING

end