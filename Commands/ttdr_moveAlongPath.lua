
function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. ",
		parameterDefs = {
            { 
				name = "atlasUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 50
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	if not parameter.atlasUnit then
		return FAILURE
	end

	local atlasUnit = parameter.atlasUnit

	if not parameter.path then
		return FAILURE
	end

	local x, y, z = Spring.GetUnitPosition(atlasUnit)
	local pathEnd = parameter.path[#parameter.path]
	local distance = math.sqrt((x - pathEnd.x) * (x - pathEnd.x) + (z - pathEnd.z) * (z - pathEnd.z))
	local unitsInRec = Spring.GetUnitsInRectangle(x-15, z-15, x+15, z+15)
	if distance <= 15 and #unitsInRec ~= 1 then
		return SUCCESS
	elseif distance <= 15 and #unitsInRec <= 1 then
		return SUCCESS -- 
	end

	
	if Spring.GetUnitCommands(atlasUnit, 0) > 0 then
		return RUNNING
	end
	
	

	--local count = 0
	for i = 1, #parameter.path do 
		Spring.GiveOrderToUnit(atlasUnit, CMD.MOVE, parameter.path[i]:AsSpringVector(), {"shift"})
	end



	return RUNNING
end