function getInfo()
	return {
		onNoUnits = SUCCESS,
        tooltip = "Move with cloasest commander. ",
        parameterDefs = {
            { 
				name = "pewee",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "commander",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
        }
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)

    if not parameter.pewee then
        return FAILURE
    end

    if not parameter.commander then
        return FAILURE
    end

    local pewee = parameter.pewee
    local com = parameter.commander

    local x,y,z = Spring.GetUnitPosition(pewee)
    --local comX, comY, comZ = Spring.GetUnitPosition(com)

    local distance = math.sqrt((x - com.x) * (x - com.x) + (z - com.z) * (z - com.z))
	if distance < 25 then
		return SUCCESS
    end

    -- Spring.GiveOrderToUnit(units[1], CMD.MOVE, {x + wind.x, y + wind.y, z + wind.z}, {})
    Spring.GiveOrderToUnit(pewee, CMD.MOVE, {com.x, com.y, com.z}, {})

    return RUNNING

end
