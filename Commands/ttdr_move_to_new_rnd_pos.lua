function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "in case position in safe area is occupied, find a new random position",
		parameterDefs = {
			{ 
				name = "thisUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "safeAreaCenter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "safeAreaRadius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)

    local thisUnit = parameter.thisUnit
    local safeAreaCenter = parameter.safeAreaCenter
    local safeAreaRadius = parameter.safeAreaRadius

    local cmdID = CMD.MOVE


	local posX, posY, posZ = Spring.GetUnitPosition(thisUnit)
	local unitPos = Vec3(posX, posY, posZ)

	local troopsOccupyingSpace = Spring.GetUnitsInRectangle(posX - 10, posZ - 10, posX + 10, posZ + 10)
	local newRndPos = nil

    while #troopsOccupyingSpace > 2 do
        newRndPos = Sensors.nota_schimper_homeworkassignments.ttdr_getRndPosInArea(safeAreaCenter, safeAreaRadius)
        Spring.GiveOrderToUnit(thisUnit, cmdID, newRndPos,  {"shift"})
		troopsOccupyingSpace =  Spring.GetUnitsInRectangle(posX - 10, posZ - 10, posX + 10, posZ + 10)
    end

	if #troopsOccupyingSpace == 2 --[[and unitPos:Distance(newRndPos) == 0]] then
		return SUCCESS
	end

	return RUNNING

end