function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
        tooltip = "Move custom group evenly accorss the map.",
        parameterDefs = {			
            { 
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "thirdparam",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Eucl_Dist(from, to)
	return math.sqrt((from.x - to.x) * (from.x - to.x) + (from.z - to.z) * (from.z - to.z))
end

function Run(self, units, parameter)

    -- store command
    local cmdID = CMD.MOVE 

    local mapSizeZ = Game.mapSizeZ

    -- transport units and amount
    local scoutID = parameter.unitID
    local finalPosition = parameter.position
    local thirdparam = parameter.thirdparam

    local x, y, z = Spring.GetUnitPosition(scoutID)
    local myPosition = Vec3(x,y,z)
    local finalPositionHeight = Spring.GetGroundHeight(finalPosition.x, finalPosition.z)

    local startPosition = Vec3(finalPosition.x, finalPositionHeight, finalPosition.z)

    SpringGiveOrderToUnit(scoutID, cmdID, startPosition:AsSpringVector(), {})
    --commandAlreadySubmitted[unitID] = true


	local distance = Eucl_Dist(myPosition, startPosition)
    if distance < 250 then
        --thirdparam[unitID] = true
        return SUCCESS
    else
        return RUNNING
    end
    
end
