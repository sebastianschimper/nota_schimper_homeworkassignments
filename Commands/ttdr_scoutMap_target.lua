function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move custom group evenly accorss the map.",
        parameterDefs = {            
            { 
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            { 
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)

    -- store map dimensions
    local mapSizeX = Game.mapSizeX 
    local mapSizeZ = Game.mapSizeZ

    -- store command
    local cmdID = CMD.MOVE 

    -- transport units and amount
    local scoutID = parameter.unitID
    local finalPosition = parameter.position

    local x, y, z = Spring.GetUnitPosition(scoutID)
    local myPosition = Vec3(x,y,z)
    
    local startPositionHeight = Spring.GetGroundHeight(finalPosition.x, finalPosition.z)
    local startPosition = Vec3(finalPosition.x, startPositionHeight, finalPosition.z)

    local targetPositionHeight = Spring.GetGroundHeight(finalPosition.x + mapSizeZ, finalPosition.z+mapSizeZ)
    local targetPosition = Vec3(finalPosition.x + mapSizeZ, targetPositionHeight, finalPosition.z + mapSizeZ)

    SpringGiveOrderToUnit(scoutID, cmdID, targetPosition, {})

    local distance = targetPosition:Distance(myPosition)
    if distance < 50 then
        return SUCCESS
    end
    
    return RUNNING

end