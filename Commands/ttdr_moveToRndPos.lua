function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. ",
		parameterDefs = {
            { 
				name = "atlasUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "rndPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)

    if not parameter.rndPos then
		return FAILURE
    end

    if not parameter.atlasUnit then
		return FAILURE
    end
    
    local atlasUnit = parameter.atlasUnit
    local rndPos = parameter.rndPos

    if (not Spring.GetUnitIsDead(atlasUnit) and #Spring.GetUnitCommands(atlasUnit) > 0) then
		return RUNNING
    end
    
    
    local distance = math.sqrt((x - rndPos.x) * (x - rndPos.x) + (z - rndPos.z) * (z - rndPos.z))
	if distance < 25 then
		return SUCCESS
    end
    
    Spring.GiveOrderToUnit(atlasUnit, CMD.MOVE, rndPos, {})

    return RUNNING

end