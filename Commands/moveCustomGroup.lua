function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},

			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits -- array
	local position = parameter.position -- Vec3

	local cmdID = CMD.MOVE
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[1])



	for i=1, #listOfUnits do
		if (i == 1) then
			SpringGiveOrderToUnit(listOfUnits[i], cmdID, {pointX + 20, pointY, pointZ+20}, {})
		else
			local leaderX, leaderY, leaderZ = SpringGetUnitPosition(listOfUnits[i-1])
			SpringGiveOrderToUnit(listOfUnits[i], cmdID, {pointX + 20, pointY, pointZ+20}, {})
		end
	end
	
	return RUNNING
end




function Reset(self)
	ClearState(self)
end
