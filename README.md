# Homework Assignments for Programming with NOTA 

This work is part of the homework for my university class called "Human-like artificial Agents". In this class, I am learning
about AI programming with Lua in the real time strategy game NOTA.

## Deployment

To be filled out ... 

## Built With

* [Lua]

## Homework Assignment 00
Seting up Development Environment, download NOTA etc. In short: getting everthing ready.

## Homework Assignment 01 - Sandsail
Develop sensor, that detects wind direction and create "Sandsail" command, which enables units to move in direction of the wind.

## Pratical Lecture 3
Practising: Write a sensor that returns enemy units

## Homework Assignment 02 - Ctp2
Create behavior that allows a given set of units to capture higher platforms in a quick manner.

## Pratical Lecture 5
Practising: Write a command that makes one "leader" tropper moves to a given direction and that makes troops follow each previous trooper.

## Homework Assignment 03 - ttdr2
Rescue stranded units and bring them to a safe area. Avoid projectiles of enemy units. Find a clever path.

## Contributing

Please see [NOTA HOMEPAGE](http://nota.machys.net/) for details on the game.

## Authors

* **Sebastian Schimper** 

See also the web page of the [course at university](https://gamedev.cuni.cz/study/courses-history/courses-2018-2019/labs-for-human-like-artificial-agents-summer-201819/)

## License
To be filled out ... 
